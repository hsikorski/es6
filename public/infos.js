

// yarn dev



const arr = [1, 3, 4, 5, 8, 9];

// map retorna um novo array
const newArr = arr.map(function(item){
  return item *2;
});
// ou
const newArr = arr.map((item) => {
  return item *2;
});
// ou
const newArr = arr.map( item => item * 2);


// o reduce resume a uma string
const newArr = arr.reduce(function(total, next){
  return total + next;
});


// o filter retorna apenas o que atende a condição
const newArr = arr.filter(function(item){
  return item % 2 === 0; //remove apenas os divisíveis por 2
});

// verificar ou encontrar uma informação dentro do array
const newArr = arr.find(function(item){
  return item === 4;
});
// criando um novo array com o map()
const usuarios = [
  { nome: 'Diego', idade: 23, empresa: 'Rocketseat' },
  { nome: 'Gabriel', idade: 15, empresa: 'Rocketseat' },
  { nome: 'Lucas', idade: 30, empresa: 'Facebook' },
 ];


 // criando um novo array com o map()
 const idades = usuarios.map(item => item.idade);
 console.log(idades);

 // criando um novo array com o filter()
 const empresa = usuarios.filter(item => item.empresa === 'Rocketseat');
 console.log(empresa);

 // criando um novo array com o find()
 const nome = usuarios.find(item => item.nome === 'Lucas');
 console.log(nome);







function teste(){
  return 'opa';
}
// ou
const teste = () => {
 return 'opa';
}
// ou 
const teste = () => 'opa';
const teste = () => [1, 2, 5];
const teste = () => true;
const teste = () => ({ nome: 'Siki' });






// quando for objeto
const teste = () => {
   return { nome: 'Siki' };
}
const teste = () => ({ nome: 'Siki' });







// valores padrão, quando não forem definidos
function soma(a = 3, b = 6){
  return a + b;
}
soma(15); //a é 15, b é 6, pois foi definido na função
//ou
const soma = (a = 3, b = 6) => a + b;





// desestruturacao 
const usuario = {
  nome: 'Henrique',
  idade: '37',
  endereco: {
    cidade: 'Rio do Sul',
    estado: 'SC',
  }
};
const nome = usuario.nome;
console.log(nome);

//ou

const { nome, idade, endereco:{ estado } } = usuario;
console.log(nome);

// ou

function mostrarNome(usuario){
  console.log(usuario.nome);
}
mostrarNome(usuario);

//ou 
function mostrarNome({ nome, idade}){
  console.log(nome);
}
mostrarNome(usuario);





// REST
const usuario = {
  nome: 'Henrique',
  idade: '37',
  endereco: {
    cidade: 'Rio do Sul',
    estado: 'SC',
  }
};
const { nome, ...resto } = usuario;
console.log(nome);
console.log(resto);

// com funcoes
function soma(a, b, c, d){
  return a+b+c+d;
}
//ou
function soma(a, ...params){
  return a+params;
}
console.log(soma(1,4,3,8));

// com array
const arr = [1, 3, 5, 8];
const [ a, b, ...c ] = arr;
console.log(a);
console.log(b);
console.log(c);




// SPREAD
const arr1 = [1, 2, 3];
const arr2 = [4, 5, 6];

const arr3 = [...arr1, ...arr2];

const usuario1 = {
  nome: 'Henrique',
  idade: '37',
};
const usuario2 = { ...usuario1, nome: 'Siki'};





// TEMPLATE LITERALS
const nome = 'Siki';
const idade = 37;
console.log('Meu nome é ' + nome + '(' + idade + ')');
//ou
console.log(`Meu nome é ${nome} (${idade})`);





// SHORT SINTAX
const nome = 'Siki';
const idade = 37;

const usuario = {
  nome: nome,
  idade: idade,
  empresa: 'Firma'
}
//ou
const usuario = {
  nome,
  idade,
  empresa: 'Firma'
}



// IMPORT
import { soma, subtracao } from './funcoes'

// quando há mais funcoes no mesmo arquivo
console.log(soma(3,3));
console.log(subtracao(3,3));

// para mudar o nome
import { soma as somaFunction, subtracao } from './funcoes'
console.log(somaFunction(3,3));

// quando há uma default e outras comuns
import soma, { subtracao } from './funcoes'
console.log(soma(3,3));
console.log(subtracao(3,3));

// quando há muitas no mesmo arquivo
import * as funcoes from './funcoes'
console.log(funcoes.soma(3,3));
console.log(funcoes.subtracao(3,3));

// ou
// quando há um export default lá no arquivo funcoes.js
// export default Soma;
import soma from './funcoes'



// classe
class TodoList {
  constructor(){
    this.todos = []
  }
}

class List extends TodoList {
  constructor(){
    super(); // para puxar as classes da classe pai/superior
  }
}









import axios from 'axios';

class Api {
  static async getUserInfo(username){
    try{
      const response = await axios.get(`https://api.github.com/users/${username}`);
      console.log(response);
    } catch (err) {
      console.warn('Erro na API');
    }
  }  
}

Api.getUserInfo('diego3g');
Api.getUserInfo('diego3gd');







const minhaPromise = () => new Promise((resolve, reject) => setTimeout(() => { resolve('ok')}, 2000));

/*
async function executaPromise(){
  console.log(await minhaPromise());
  console.log(await minhaPromise());
  console.log(await minhaPromise());
}
*/
//ou
// awai somente dentro de um async
const executaPromise = async () => {
  console.log(await minhaPromise());
  console.log(await minhaPromise());
  console.log(await minhaPromise());
}

executaPromise();


/*

async function executaPromise(){
  minhaPromise().then(response => {
    console.log(response);

    minhaPromise().then(response => {
      console.log(response);

      minhaPromise().then(response => {
        console.log(response);
      })
    })
  })
}

*/